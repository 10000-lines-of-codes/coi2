import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'

import Danhmucma from '@/components/Danhmucma'
import Giaodich from '@/components/Giaodich'
import Lichsu from '@/components/Lichsu'
import GDtotnhat from '@/components/GDtotnhat'
import Ruttien from '@/components/Ruttien'
import Khuyenmai from '@/components/Khuyenmai'
import Tintuc from '@/components/Tintuc'
import Dautu from '@/components/Dautu'
import Napthem from '@/components/Napthem'

import Taikhoan from '@/components/Taikhoan'
import Thongtin from '@/components/Thongtin'

Vue.use(Router)

export default new Router({
    mode: 'history',
  routes: [
    {
      path: '/',
	name: 'Home',
	component: Home
    },
      {
	  path: '/danhmucma',
	  name: 'Danhmucma',
	  component: Danhmucma
      },
      {
	  path: '/giaodich',
	  name: 'Giaodich',
	  component: Giaodich
      },
      {
	  path: '/lichsu',
	  name: 'Lichsu',
	  component: Lichsu
      },
      {
	  path: '/gdtotnhat',
	  name: 'GDtotnhat',
	  component: GDtotnhat,
      },
      {
	  path: '/ruttien',
	  name: 'Ruttien',
	  component: Ruttien
      },
      {
	  path: '/khuyenmai',
	  name: 'Khuyenmai',
	  component: Khuyenmai
      },
      {
	  path: '/tintuc',
	  name: 'Tintuc',
	  component: Tintuc
      },
      {
	  path: '/dautu',
	  name: 'Dautu',
	  component: Dautu
      },
      {
	  path: '/napthem',
	  name: 'Napthem',
	  component: Napthem
      },
      {
	  path: '/taikhoan',
	  name: 'Taikhoan',
	  component: Taikhoan
      },
      {
	  path: '/thongtin',
	  name: 'Thongtin',
	  component: Thongtin
      }
      
  ]
})
